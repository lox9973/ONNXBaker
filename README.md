# ONNXBaker

ONNXBaker converts ("bake") a ONNX model to Unity textures, materials and shaders, so the neural network can be inferenced by simply calling `Graphics.Blit`.

## Installation

An empty Unity 2021.3 project is recommended. Baking is tested on Unity 2021.3, and inference is tested on Unity 2021.3 and 2019.4. Baking on 2019.4 is not recommended for performance reason.

1. Install [Harmony](https://github.com/pardeike/Harmony)

    Either [download](https://github.com/pardeike/Harmony/releases/) `0Harmony.dll` into your Unity project, or [install](https://docs.unity3d.com/Manual/upm-ui-giturl.html) the package [harmony-vpm](https://github.com/rrazgriz/harmony-vpm) from [this Git url](https://github.com/rrazgriz/harmony-vpm.git?path=Packages/one.razgriz.harmony-vpm).

2. Install [Barracuda](https://docs.unity3d.com/Packages/com.unity.barracuda@3.0/)

    [Download](https://github.com/Unity-Technologies/barracuda-release/releases/tag/3.0.0) Unity Barracuda 3.0.0, and [install](https://docs.unity3d.com/Manual/upm-ui-local.html) the package from a local folder. Make sure it is NOT installed in `Library/PackageCache`. Run the following command in [bash](https://learn.microsoft.com/en-us/windows/wsl/tutorials/linux) to apply the [patch file](barracuda-3.0.0.patch).

```bash
patch -p0 -d [BARRACUDA_INSTALLATION_FOLDER] < barracuda-3.0.0.patch
```

3. Import

    Download this repository into your Unity project, under an appropriate folder like `/Assets/ONNXBaker`.

## ONNX model

If you are new to ONNX or Barracuda, **please refer to the [example scene](Example/MiDaS/Example.unity)** for a sample of running a [MiDaS](https://github.com/isl-org/MiDaS) model with Barracuda.

In general, ONNXBaker requires an ONNX model supported by Barracuda pixel shader [worker](https://docs.unity3d.com/Packages/com.unity.barracuda@3.0/manual/Worker.html). Most models with [ONNX opset](https://github.com/onnx/onnx/blob/main/docs/Operators.md) 10 should work.


## Baking

Bake by recording [Graphics.Blit](https://docs.unity3d.com/ScriptReference/Graphics.Blit.html) calls in the execution of a Barracuda worker:

```cs
// load ONNX model and create Barracuda worker in pixel shader mode
var model = ModelLoader.Load(onnxModel);
var worker = WorkerFactory.CreateWorker(WorkerFactory.Type.PixelShader, model);

// record and run the worker
ONNXBaker.Recorder.Start();
using(var tensor = new Tensor(source, 3)) {
	worker.Execute(tensor);
	worker.PeekOutput();
}
ONNXBaker.Recorder.Stop();

// generate materials, textures and c# code
ONNXBaker.Exporter.ExportAsset("Assets/BakedONNX/BakedONNX.cs");
ONNXBaker.Recorder.Clear();
```

The exporter generates the following code with material and texture assets in the specified folder:

```cs
public class BakedONNX : MonoBehaviour {
	public Material[] mat = new Material[195];
	public Texture[] tex = new Texture[198];
	public RenderTexture[] rt = new RenderTexture[70];
	void LoadModel() {
		mat[0].SetFloat("_FlipY", 1f);
		mat[0].SetVector("OdeclShape", new Vector4(1f, 256f, 256f, 3f));
		mat[0].SetVector("_Scale", new Vector4(1f, 1f, 1f, 1f));
		mat[0].SetVector("_Bias", new Vector4(0f, 0f, 0f, 0f));
		mat[0].SetVector("_Pool", new Vector4(256f, 256f, 0f, 0f));
		mat[0].SetVector("_Pad", new Vector4(0f, 0f, 0f, 0f));
		mat[0].SetVector("_ChannelWriteMask", new Vector4(1f, 1f, 1f, 0f));
		mat[0].SetVector("_ChannelWriteMap", new Vector4(0f, 1f, 2f, 3f));
		mat[0].SetVector("_ChannelReadMap", new Vector4(0f, 1f, 2f, -1f));
		mat[0].SetTexture("Xtex2D", tex[0]);
		// ...
	}
	void RunModel() {
		Graphics.Blit(null, rt[0], mat[0]);
		Graphics.Blit(null, rt[1], mat[1]);
		Graphics.Blit(null, rt[0], mat[2]);
		Graphics.Blit(null, rt[2], mat[3]);
		// ...
	}
}
```

## Inference

Harmony and Barracuda runtime are not needed for inference. You only need to import all shaders from `Barracuda/Runtime/Core/Resources/Barracuda/PixelShaders`.

Before running the BakedONNX component, you need to populate the arrays `mat`, `tex`, `rt` with generated assets:

```cs
var baked = onnxGameObject.GetComponent<BakedONNX>();
ONNXBaker.Exporter.CopyFields(baked, "Assets/BakedONNX");
```

## Credits

- [Unity Barracuda](https://github.com/Unity-Technologies/barracuda-release) for making possible this project
- [monocular-depth-unity](https://github.com/GeorgeAdamon/monocular-depth-unity) for an example of ONNX
- [Harmony](https://github.com/pardeike/Harmony) for the Harmony library
- [RATS](https://github.com/rrazgriz/RATS) for an example of patching
- [SCRN](https://github.com/SCRN-VRC) for discussion on ONNX
