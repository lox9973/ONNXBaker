#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using HarmonyLib;
using System.Reflection;

namespace ONNXBaker {
[InitializeOnLoad]
public class Patcher {
	[HarmonyPatch]
	class PatchMaterialSetInt {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Material), "SetInt",
			new System.Type[]{typeof(string), typeof(int)});
		static void Postfix(Material __instance, string name, int value)
			=> Recorder.PostMaterialSetFloat(__instance, name, (float)value);
	}
	[HarmonyPatch]
	class PatchMaterialSetFloat {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Material), "SetFloat",
			new System.Type[]{typeof(string), typeof(float)});
		static void Postfix(Material __instance, string name, float value)
			=> Recorder.PostMaterialSetFloat(__instance, name, value);
	}
	[HarmonyPatch]
	class PatchMaterialSetVector {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Material), "SetVector",
			new System.Type[]{typeof(string), typeof(Vector4)});
		static void Postfix(Material __instance, string name, Vector4 value)
			=> Recorder.PostMaterialSetVector(__instance, name, value);
	}
	[HarmonyPatch]
	class PatchMaterialSetTexture {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Material), "SetTexture",
			new System.Type[]{typeof(string), typeof(Texture)});
		static void Postfix(Material __instance, string name, Texture value)
			=> Recorder.PostMaterialSetTexture(__instance, name, value);
	}
	[HarmonyPatch]
	class PatchGraphicsBlit3 {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Graphics), "Blit",
			new System.Type[]{typeof(Texture), typeof(RenderTexture), typeof(Material)});
		static void Postfix(Texture source, RenderTexture dest, Material mat)
			=> Recorder.PostGraphicsBlit(source, dest, mat);
	}
	[HarmonyPatch]
	class PatchGraphicsBlit2 {
		static MethodBase TargetMethod() => AccessTools.Method(typeof(Graphics), "Blit",
			new System.Type[]{typeof(Texture), typeof(RenderTexture)});
		static void Postfix(Texture source, RenderTexture dest)
			=> Recorder.PostGraphicsBlit(source, dest);
	}

	// modified from https://github.com/rrazgriz/RATS/blob/main/Editor/RATS.cs
	static Patcher() {
		EditorApplication.update -= DoPatches;
		EditorApplication.update += DoPatches;
	}
	static Harmony harmonyInstance = new Harmony("ONNXBaker");
	static int wait = 0;
	static void DoPatches() {
		wait++;
		if(wait > 2) {
			EditorApplication.update -= DoPatches;
			harmonyInstance.PatchAll();
			Debug.Log($"${harmonyInstance.Id} patch is applied");
		}
	}
}
}
#endif