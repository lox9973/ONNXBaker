#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace ONNXBaker {
[InitializeOnLoad]
public class Recorder {
	private static bool running = false;
	public static void Start(bool codeOnly=false) {
		Clear();
		assets.mock = codeOnly;
		running = true;
	}
	public static void Stop() {
		running = false;
	}
	public static void Clear() {
		materialCache.Clear();
		blitTexIds.Clear();
		blits.Clear();
		assets.Clear();
	}

	public class PropertyBlock {
		public Dictionary<string, float> floats = new();
		public Dictionary<string, Vector4> vectors = new();
		public Dictionary<string, Texture> textures = new();
	};
	private static Dictionary<int, PropertyBlock> materialCache = new();
	internal static void PostMaterialSetFloat(Material __instance, string name, float value) {
		if(running)
			materialCacheGet(__instance).floats[name] = value;
	}
	internal static void PostMaterialSetVector(Material __instance, string name, Vector4 value) {
		if(running)
			materialCacheGet(__instance).vectors[name] = value;
	}
	internal static void PostMaterialSetTexture(Material __instance, string name, Texture value) {
		if(running)
			materialCacheGet(__instance).textures[name] = value;
	}
	private static PropertyBlock materialCacheGet(Material mat) {
		if(!materialCache.TryGetValue(mat.GetInstanceID(), out var prop))
			materialCache[mat.GetInstanceID()] = prop = new PropertyBlock();
		return prop;
	}

	public class BlitCall {
		public RenderTextureDescriptor descriptor;
		public Shader shader;
		public PropertyBlock propBlock;
		public int materialId;
		public Dictionary<string, int> textureIds = new();
	};
	private static Dictionary<Texture, int> blitTexIds = new();
	public static List<BlitCall> blits = new();
	public static AssetContainer assets = new();
	public const int FLAG_NOT_RT = 1<<30;
	internal static void PostGraphicsBlit(Texture source, RenderTexture dest, Material mat=null) {
		if(running) {
			blitTexIds[dest] = blits.Count; // self-reference can occur
			var blit = new BlitCall();
			blit.descriptor = dest.descriptor;
			blit.shader = mat ? mat.shader : null;
			blit.propBlock = mat ? materialCacheGet(mat) : new();
			if(source)
				blit.propBlock.textures["_MainTex"] = source;
			blit.materialId = assets.AddMaterial(mat);
			foreach(var (name, tex) in blit.propBlock.textures)
				if(tex) {
					if(!blitTexIds.TryGetValue(tex, out var texId))
						texId = assets.AddTexture(tex) | FLAG_NOT_RT;
					blit.textureIds[name] = texId;
				}
			blits.Add(blit);
			// clear references
			blit.propBlock.textures.Clear();
			blit.propBlock.textures = null;
			materialCache.Clear();
		}
	}
}
}
#endif