#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using System.Collections.Generic;

namespace ONNXBaker {
public class AssetContainer {
	public bool mock;
	public void Clear() {
		materials.Clear();
		textures.Clear();
		renderTextures.Clear();
		renderStates.Clear();
	}

	List<Material> materials = new();
	public IReadOnlyList<Material> GetMaterials() => materials;
	public int AddMaterial(Material mat) {
		if(mock) {
			materials.Add(null);
		} else {
			var clone = new Material(mat ? mat.shader : Shader.Find("Hidden/BlitCopy"));
			if(mat)
				clone.CopyPropertiesFromMaterial(mat); // keywords are copied
			materials.Add(clone);
		}
		return materials.Count-1;
	}

	List<Texture> textures = new();
	public IReadOnlyList<Texture> GetTextures() => textures;
	public int AddTexture(Texture tex) {
		var rt = tex as RenderTexture;
		var tex2D = tex as Texture2D;
		if(mock) {
			textures.Add(null);
		} else if(rt) {
			var clone = new Texture2D(tex.width, tex.height, tex.graphicsFormat, TextureCreationFlags.None);
			var activeRT = RenderTexture.active;
			RenderTexture.active = rt;
			clone.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
			clone.Apply();
			RenderTexture.active = activeRT;
			textures.Add(clone);
		} else if(tex2D) {
			var clone = new Texture2D(tex.width, tex.height, tex.graphicsFormat, TextureCreationFlags.None);
			Graphics.CopyTexture(tex, clone);
			textures.Add(clone);
		} else
			return -1;
		return textures.Count-1;
	}

	List<RenderTexture> renderTextures = new();
	List<(RenderTextureDescriptor, bool)> renderStates = new();
	public IReadOnlyList<RenderTexture> GetRenderTextures() => renderTextures;
	public int AddRenderTexture(RenderTextureDescriptor desc) {
		int i=0;
		foreach(var (x, free) in renderStates) { // TODO: speedup
			if(free && x.width == desc.width && x.height == desc.height && x.graphicsFormat == desc.graphicsFormat) {
				renderStates[i] = (x, false);
				return i;
			}
			i++;
		}
		if(mock) {
			renderTextures.Add(null);
		} else {
			renderTextures.Add(new RenderTexture(desc));
		}
		renderStates.Add((desc, false));
		return renderTextures.Count-1;	
	}
	public void ReleaseRenderTexture(int i) {
		renderStates[i] = (renderStates[i].Item1, true);
	}
}
}
#endif