#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;

namespace ONNXBaker {
public class Exporter {
	const string MAT = "mat";
	const string TEX = "tex";
	const string RT  = "rt";

	public static void ExportAsset(string path) {
		var blits = Recorder.blits;
		var releaseTime = new int[blits.Count];
		for(int i=0; i<blits.Count; i++) {
			releaseTime[i] = blits.Count-1;
			foreach(var (_, texId) in blits[i].textureIds)
				if((texId & Recorder.FLAG_NOT_RT) == 0)
					releaseTime[texId] = i;
		}
		var renderTexIds = new int[blits.Count];
		for(int i=0; i<blits.Count; i++) {
			renderTexIds[i] = Recorder.assets.AddRenderTexture(blits[i].descriptor);
			for(int j=0; j<blits.Count; j++) // TODO: speedup
				if(releaseTime[j] == i)
					Recorder.assets.ReleaseRenderTexture(renderTexIds[j]);
		}

		var maybeInputs = new List<int>();
		var maybeOutputs = new List<int>();
		for(int i=0; i<blits.Count; i++) {
			var shader = blits[i].shader;
			if(shader) {
				if(shader.name == "Barracuda/TextureToTensor" || shader.name == "Barracuda/BufferToTensor")
					if(blits[i].textureIds.TryGetValue("Xtex2D", out var texId))
						maybeInputs.Add(texId);
				if(shader.name == "Barracuda/TensorToBuffer" || shader.name == "Barracuda/TensorToTexture")
					if(blits[i].textureIds.TryGetValue("Xdata", out var texId))
						maybeOutputs.Add(texId);
			}
		}

		var folder = System.IO.Path.GetDirectoryName(path);
		var stem = System.IO.Path.GetFileNameWithoutExtension(path);
		System.IO.Directory.CreateDirectory(folder);
		var mats = Recorder.assets.GetMaterials();
		var texs = Recorder.assets.GetTextures();
		var rts  = Recorder.assets.GetRenderTextures();
		using (var w = new System.IO.StreamWriter(path)) {
			w.WriteLine("using UnityEngine;");
			w.WriteLine($"public class {stem} : MonoBehaviour {{");
			w.WriteLine("\tvoid Start() {");
			w.WriteLine($"\t\t// TODO: Handle inputs here. For example, modify: {string.Join(", ", maybeInputs.ConvertAll(x => exprTexId(x, renderTexIds)))}");
			w.WriteLine("\t\tLoadModel();");
			w.WriteLine("\t}");
			w.WriteLine("\tvoid Update() {");
			w.WriteLine("\t\tRunModel();");
			w.WriteLine($"\t\t// TODO: Handle outputs here. For example, read from: {string.Join(", ", maybeOutputs.ConvertAll(x => exprTexId(x, renderTexIds)))}");
			w.WriteLine("\t}");
			w.WriteLine("\t// TODO: Initialize these fields. For example, call Exporter.CopyFields");
			w.WriteLine($"\tpublic Material[] {MAT} = new Material[{mats.Count}];");
			w.WriteLine($"\tpublic Texture[] {TEX} = new Texture[{texs.Count}];");
			w.WriteLine($"\tpublic RenderTexture[] {RT} = new RenderTexture[{rts.Count}];");
			w.WriteLine("\tvoid LoadModel() {");
			for(int i=0; i<blits.Count; i++) {
				var shader = blits[i].shader;
				var desc = blits[i].descriptor;
				w.WriteLine($"\t\t// {(shader?shader.name:"")} => RenderTexture({desc.width}, {desc.height}, {desc.colorFormat})");
				foreach(var (name, val) in blits[i].propBlock.floats)
					w.WriteLine($"\t\t{MAT}[{blits[i].materialId}].SetFloat({exprString(name)}, {exprFloat(val)});");
				foreach(var (name, val) in blits[i].propBlock.vectors)
					w.WriteLine($"\t\t{MAT}[{blits[i].materialId}].SetVector({exprString(name)}, {exprVector(val)});");
				foreach(var (name, val) in blits[i].textureIds)
					w.WriteLine($"\t\t{MAT}[{blits[i].materialId}].SetTexture({exprString(name)}, {exprTexId(val, renderTexIds)});");
			}
			w.WriteLine("\t}");
			w.WriteLine("\tvoid RunModel() {");
			for(int i=0; i<blits.Count; i++) {
				w.WriteLine($"\t\tGraphics.Blit(null, {RT}[{renderTexIds[i]}], {MAT}[{blits[i].materialId}]);");
			}
			w.WriteLine("\t}");
			w.WriteLine("}");
		}
		if(!Recorder.assets.mock) {
			try {
				AssetDatabase.StartAssetEditing();
				System.IO.Directory.CreateDirectory($"{folder}/{MAT}");
				System.IO.Directory.CreateDirectory($"{folder}/{TEX}");
				System.IO.Directory.CreateDirectory($"{folder}/{RT}");
				for(int i=0; i<mats.Count; i++)
					AssetDatabase.CreateAsset(mats[i], $"{folder}/{MAT}/{i}.asset");
				for(int i=0; i<texs.Count; i++)
					AssetDatabase.CreateAsset(texs[i], $"{folder}/{TEX}/{i}.asset");
				for(int i=0; i<rts.Count; i++)
					AssetDatabase.CreateAsset(rts[i], $"{folder}/{RT}/{i}.asset");
			} finally {
				AssetDatabase.StopAssetEditing();
			}
		}
	}
	static string exprString(string v) => $"\"{v}\"";
	static string exprFloat(float v) => $"{v}f";
	static string exprVector(Vector4 v)
		=> $"new Vector4({exprFloat(v.x)}, {exprFloat(v.y)}, {exprFloat(v.z)}, {exprFloat(v.w)})";
	static string exprTexId(int id, int[] renderTexIds) {
		if(id < 0)
			return "null";
		else if((id & Recorder.FLAG_NOT_RT) != 0)
			return $"{TEX}[{id&(Recorder.FLAG_NOT_RT-1)}]";
		else
			return $"{RT}[{renderTexIds[id]}]";
	}

	public static void CopyFields(MonoBehaviour o, string folder) {
		var matField = o.GetType().GetField(MAT);
		var texField = o.GetType().GetField(TEX);
		var rtField  = o.GetType().GetField(RT);
		var mat = (Material[])matField.GetValue(o);
		var tex = (Texture[])texField.GetValue(o);
		var rt  = (RenderTexture[])rtField.GetValue(o);
		Debug.Log($"#mat={mat.Length}, #tex={tex.Length}, #rt={rt.Length}");

		for(int i=0; i<mat.Length; i++)
			mat[i] = AssetDatabase.LoadAssetAtPath<Material>($"{folder}/{MAT}/{i}.asset");
		for(int i=0; i<tex.Length; i++)
			tex[i] = AssetDatabase.LoadAssetAtPath<Texture>($"{folder}/{TEX}/{i}.asset");
		for(int i=0; i<rt.Length; i++)
			rt[i]  = AssetDatabase.LoadAssetAtPath<RenderTexture>($"{folder}/{RT}/{i}.asset");
	}
	[MenuItem("CONTEXT/MonoBehaviour/ONNXBaker - CopyFields")]
	static void CopyFields(MenuCommand command) {
		var o = (MonoBehaviour)command.context;
		var script = MonoScript.FromMonoBehaviour(o);
		var folder = System.IO.Path.GetDirectoryName(AssetDatabase.GetAssetPath(script));
		CopyFields(o, folder);
	}
}
}
#endif