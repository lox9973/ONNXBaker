// modified from https://github.com/GeorgeAdamon/monocular-depth-unity/blob/main/MonocularDepthBarracuda/Packages/DepthFromImage/Runtime/DepthFromImage.cs
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using Unity.Barracuda;

namespace ONNXBaker.Example {
public class DepthFromImage : MonoBehaviour {
	public WorkerFactory.Type workerType = WorkerFactory.Type.Auto;
	public NNModel neuralNetworkModel;
	public Texture inputTexture;
	public RenderTexture outputTexture;
	public Material outputMaterial;
	public bool bakeModel = true;
	public bool codeOnly = true;

	private Model         _model;
	private IWorker       _engine;
	private RenderTexture _input, _output;
	private int           _width, _height;
	private PixelShaderOps gpuBackend;

	private void Start() {
		InitializeNetwork();
		AllocateObjects();
	}

	private void Update() {
		if (inputTexture == null)
			return;
		
		if (neuralNetworkModel == null)
			return;
		
		Graphics.Blit(inputTexture, _input);

		var baking = bakeModel && workerType == WorkerFactory.Type.PixelShader;
		if(baking) {
			Recorder.Start(codeOnly);
		}

		RunModel(_input);

		if(baking) {
			Recorder.Stop();
			if(Recorder.blits.Count > 0) {
				var path = "Assets/BakedONNX/BakedONNX.cs~";
				Exporter.ExportAsset(path);
				EditorUtility.DisplayDialog("Baking complete", $"Exported to {path}", "OK");
				bakeModel = false;
			}
			Recorder.Clear();
		}

		Graphics.Blit(_output, outputTexture, outputMaterial);
	}

	private void OnDestroy() {
		DeallocateObjects();
	}

	private void InitializeNetwork() {
		if (neuralNetworkModel == null)
			return;

		_model = ModelLoader.Load(neuralNetworkModel);
		_engine = WorkerFactory.CreateWorker(workerType, _model);
		_width  = _model.inputs[0].shape[5];
		_height = _model.inputs[0].shape[6];
		gpuBackend = new PixelShaderOps(null);
	}

	private void AllocateObjects() {
		if (inputTexture == null)
			return;

		// Check for accidental memory leaks
		_input?.Release();
		_output?.Release();
		
		// Declare texture resources
		_input  = new RenderTexture(_width, _height, 0, RenderTextureFormat.ARGB32);
		_output = new RenderTexture(_width, _height, 0, RenderTextureFormat.RFloat);
		
		// Initialize memory
		_input.Create();
		_output.Create();
	}

	private void DeallocateObjects() {
		_engine?.Dispose();
		_engine = null;
		_input?.Release();
		_input = null;
		_output?.Release();
		_output = null;
	}

	private void RunModel(Texture source) {
		using (var tensor = new Tensor(source, 3))
			_engine.Execute(tensor);

		var to = _engine.PeekOutput();
		to.DetachFromDevice(); // fix transpose shape issue
		to = to.Reshape(new TensorShape(1, _width, _height, 1)); // fix incorrectly imported output shape
		gpuBackend.TensorToRenderTexture(to, _output, batch:0, fromChannel:0, scale:Vector4.one, bias:Vector4.zero, lut: null);
	}
}
}
#endif