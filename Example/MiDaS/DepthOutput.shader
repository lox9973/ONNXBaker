Shader "MiDaS/DepthOutput" {
Properties {
	_MainTex("_MainTex", 2D) = "black" {}
	_DepthScale("_DepthScale", Float) = 0.001
}
SubShader {
	Tags { "RenderType"="Opaque" }
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#include "UnityCG.cginc"

		sampler2D _MainTex;
		float _DepthScale;

		struct FragInput {
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};
		void vert(appdata_base v, out FragInput o) {
			o.uv = float2(1-v.texcoord.y, 1-v.texcoord.x);
			// o.uv = float2(v.texcoord.x, 1-v.texcoord.y);
			o.vertex = UnityObjectToClipPos(v.vertex);
		}
		float4 frag(FragInput i) : SV_Target {
			float depth = tex2D(_MainTex, i.uv).r;
			depth *= _DepthScale;
			return float4(depth*depth*float3(1,1,1), 1);
		}
		ENDCG
	}
}
}
